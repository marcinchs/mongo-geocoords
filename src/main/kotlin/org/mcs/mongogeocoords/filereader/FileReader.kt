package org.mcs.mongogeocoords.filereader

import org.mcs.mongogeocoords.logger
import org.mcs.mongogeocoords.dao.GeoObjectRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.stereotype.Component
import java.io.File

@Component
class FileReader(
        @Value("\${path.to.input.file}") val allCountriesFile: String,
        @Value("\${path.to.featureCodes.file}") val featureCodesFile: String,
        val dbHandler: GeoObjectRepository
) : CommandLineRunner {

    val log by logger()
    val featureCodesMap: MutableMap<String, String> = mutableMapOf()

    private val batchSize = 100000
    var savedDocuments = 0

    override fun run(vararg args: String?) {
        readFeatureCodesFile()
        readAndSaveAllCountriesFile("geolocation")

    }

    fun readFeatureCodesFile() {
        File(featureCodesFile).bufferedReader().useLines {
            it.forEach {
                val list = it.split("\t")
                featureCodesMap.putIfAbsent(list[0], list[1])
            }
        }
    }

    fun readAndSaveAllCountriesFile(indexName: String) {
        File(allCountriesFile).bufferedReader().useLines {
            val arr: MutableList<GeoObject> = mutableListOf()
            it.forEach {
                val list = it.split("\t")
                val geoObject = mapGeoObject(list)
                arr.add(geoObject)
                if (arr.size >= batchSize) {
                    saveDocuments(arr)
                }
            }
            saveDocuments(arr)
        }
    }

    private fun saveDocuments(arr: MutableList<GeoObject>) {
        dbHandler.saveAll(arr)
        savedDocuments += arr.size
        log.info("Saved ${arr.size} documents. $savedDocuments total")
        arr.clear()
    }

    private fun mapGeoObject(list: List<String>): GeoObject {
        return GeoObject(
                name = list[1],
                location = GeoJsonPoint(list[5].toDouble(), list[4].toDouble()),
                objectType = featureCodesMap.getOrDefault("${list[6]}.${list[7]}", null),
                countryCode = list[8],
                altCountryCode = list[9],
                timezone = list[17]
        )
    }
}

@Document(collection = "geolocation")
data class GeoObject(
        @Id
        val id: String? = null,
        val name: String?,
        @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
        val location: GeoJsonPoint,
        val objectType: String?,
        val countryCode: String?,
        val altCountryCode: String?,
        val timezone: String?
)

