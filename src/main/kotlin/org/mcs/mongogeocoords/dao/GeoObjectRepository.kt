package org.mcs.mongogeocoords.dao

import org.mcs.mongogeocoords.filereader.GeoObject
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Component

@Component
interface GeoObjectRepository : MongoRepository<GeoObject, String>