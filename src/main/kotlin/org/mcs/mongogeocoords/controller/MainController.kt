package org.mcs.mongogeocoords.controller

import org.mcs.mongogeocoords.dao.GeoObjectRepository
import org.mcs.mongogeocoords.filereader.GeoObject
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.random.Random


@RestController
class MainController(
        val geoObjectRepository: GeoObjectRepository
) {

    @GetMapping("/getDbStat")
    fun getDbStat(): String {
        return "Indexed ${geoObjectRepository.count()} documents"
    }
}